﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wall_Theme : MonoBehaviour
{
    public GameObject wall1, wall2, wall3,panel;
    public Color HotTheme;
    public Color DefultTheme;
    public Color CoolTheme;
    private Renderer sr1,sr2,sr3;
    private Image sr4;
    public Material cold, lava, Defult;
    // Start is called before the first frame update
    void Start()
    {
        sr1 = wall1.GetComponent<Renderer>();
        sr2 = wall2.GetComponent<Renderer>();
        sr3 = wall3.GetComponent<Renderer>();
        sr4 = panel.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
  
        switch (BrickManager.Instance.theme)
        {
            case 1:
                this.sr1.material.color = HotTheme;
                this.sr1.material.SetColor("_EmissionColor", HotTheme);
                this.sr1.sharedMaterial = lava;
                this.sr2.material.color = HotTheme;
                this.sr2.material.SetColor("_EmissionColor", HotTheme);
                this.sr2.sharedMaterial = lava;
                this.sr3.material.color = HotTheme;
                this.sr3.material.SetColor("_EmissionColor", HotTheme);
                this.sr3.sharedMaterial = lava;
                this.sr4.color = HotTheme;
                this.sr4.material.SetColor("_EmissionColor", HotTheme);
                break;
            case 2:
                this.sr1.material.color = CoolTheme;
                this.sr1.material.SetColor("_EmissionColor", CoolTheme);
                this.sr1.sharedMaterial = cold;
                this.sr2.material.color = CoolTheme;
                this.sr2.material.SetColor("_EmissionColor", CoolTheme);
                this.sr2.sharedMaterial = cold;
                this.sr3.material.color = CoolTheme;
                this.sr3.material.SetColor("_EmissionColor", CoolTheme);
                this.sr3.sharedMaterial = cold;
                this.sr4.color = CoolTheme;
                this.sr4.material.SetColor("_EmissionColor", CoolTheme);
                break;
            default:
                this.sr1.material.color = DefultTheme;
                this.sr1.material.SetColor("_EmissionColor", DefultTheme);
                this.sr1.sharedMaterial = Defult;
                this.sr2.material.color = DefultTheme;
                this.sr2.material.SetColor("_EmissionColor", DefultTheme);
                this.sr2.sharedMaterial = Defult;
                this.sr3.material.color = DefultTheme;
                this.sr3.material.SetColor("_EmissionColor", DefultTheme);
                this.sr3.sharedMaterial = Defult;
                this.sr4.color = DefultTheme;
                this.sr4.material.SetColor("_EmissionColor", DefultTheme);
                break;

        }
    }
}
