﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    public bool islightingBall;
    private MeshRenderer sr;
    private Renderer SRR;
    public float ballduration = 10;
    public GameObject balleff, balleff1, balleff2;
    public static event Action<ball> OnBallDeath;
    public static event Action<ball> OnLightingBallEnable;
    public static event Action<ball> OnLightingBallDisable;
    public Material ballMatt, ballMatt1, ballMatt2;
    public AudioSource[] sound;
    private void Awake()
    {
        this.sr = GetComponentInChildren<MeshRenderer>();
        this.SRR = GetComponentInChildren<Renderer>();
    }
    private void Update()
    {
       
    }
    public void die()
    {
        OnBallDeath?.Invoke(this);
        Destroy(gameObject, 1);
    }
    public  void StartLightingBall()
    {      
        if (!this.islightingBall)
        {
            this.islightingBall = true;
            this.sr.enabled = false;
            
            switch (BrickManager.Instance.theme)
            {
                case 1:
                    balleff1.gameObject.SetActive(true);    
                    break;
                case 2:
                    balleff2.gameObject.SetActive(true);                 
                    break;
                default:
                    balleff.gameObject.SetActive(true);   
                    break;

            }            
            StartCoroutine(StopLightingBallafterTime(this.ballduration));
            OnLightingBallEnable?.Invoke(this);
        }
    }
    private IEnumerator StopLightingBallafterTime(float second)
    {
        yield return new WaitForSeconds(second);
        StopLightingBall();
    }

    private void StopLightingBall()
    {
        if (this.islightingBall)
        {
            this.islightingBall = false;
            this.sr.enabled = true;
            switch (BrickManager.Instance.theme)
            {
                case 1:
                    balleff1.gameObject.SetActive(false);
                    break;
                case 2:
                    balleff2.gameObject.SetActive(false);
                    break;
                default:
                    balleff.gameObject.SetActive(false);
                    break;

            }
            OnLightingBallDisable?.Invoke(this);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        int i = UnityEngine.Random.Range(0, sound.Length); 
        sound[i].Play();
    }
}
