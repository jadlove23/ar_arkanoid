﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathWall : MonoBehaviour
{
    public AudioSource sound;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ball")
        {
            sound.Play();
            ball ball = collision.GetComponent<ball>();
            BallManager.Instance.balls.Remove(ball);
            ball.die();
        }
    }
}
