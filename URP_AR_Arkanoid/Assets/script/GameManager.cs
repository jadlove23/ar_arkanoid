﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region sigleton

    private static GameManager _instance;
    public static GameManager Instance => _instance;
    private void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion 
    public GameObject gameoverScreen;
    public GameObject gameWinScreen;
    public GameObject lostScreen;
    public int AvailibleLives = 3;

    public GameObject paddle;
    public int Lives { get; set; }
    private bool foundPaddle = false;
    public bool IsgameStart  { get; set; }
    public static event Action<int> OnLiveLost;

    private void Start()
    {
        this.Lives = this.AvailibleLives;
        Screen.SetResolution(540, 960, false);
        ball.OnBallDeath += OnBallDeath;
        Brick.onBrickDestruction += onBrickDestruction;
    }

    private void onBrickDestruction(Brick obj)
    {
        if (BrickManager.Instance.ReminigBricks.Count <= 0)
        {
            BallManager.Instance.ResetBalls();
            GameManager.Instance.IsgameStart = false;
            BrickManager.Instance.NextLevel();
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void OnBallDeath(ball obj)
    {
        if (BallManager.Instance.balls.Count <= 0)
        {
            this.Lives--;

            if (this.Lives < 1)
            {
                //gameover
                gameoverScreen.SetActive(true);
            }
            else
            {
                //stopgame 
                //reloadscene
                OnLiveLost.Invoke(this.Lives);
                BallManager.Instance.ResetBalls();
                IsgameStart = false;
                BrickManager.Instance.LoadLevel(BrickManager.Instance.CurrentLevel);
            }
        }
    }

    internal void ShowWinScene()
    {
        gameWinScreen.SetActive(true);
    }

    private void OnDisable()
    {
        ball.OnBallDeath -= OnBallDeath;
    }
    private void Update()
    {
        if (foundPaddle == true && BrickManager.Instance.foundBricks == true )
        {
            Time.timeScale = 1;
            lostScreen.SetActive(false);
        }
        else
        {
            Time.timeScale = 0;
            lostScreen.SetActive(true);
        }
    }
    public void OnFoundPaddle()
    {
        paddle.gameObject.SetActive(true);
        foundPaddle = true;
    }
    public void OnLostPaddle()
    {
        paddle.gameObject.SetActive(false);
        foundPaddle = false;

    }
    public void OnFoundBrick()
    {
      
        BrickManager.Instance.foundBricks = true;

    }
    public void OnLostBrick()
    {
   
        BrickManager.Instance.foundBricks = false;
    }

    public void SetThemeHot()
    {
        BrickManager.Instance.theme = 1;
        Debug.Log("set theme hot");
    }
    public void SetThemeCool()
    {
        BrickManager.Instance.theme = 2;
        Debug.Log("set theme cool");
    }
    public void SetThemeDefult()
    {
        BrickManager.Instance.theme = 0;
        Debug.Log("set theme defult ");
    }
}
