﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrickManager : MonoBehaviour
{
    #region sigleton
    public bool foundBricks = false;
    private static BrickManager _instance;
    public static BrickManager Instance => _instance;
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion
    private int maxRow = 17;
    private int maxCols = 12;
    public GameObject brickContainer;
    private float intialBrickSpwanPositionX = -1.96f;
    private float intialBrickSpwanPositionY = 4.312f;
    private float shiftAmount = 0.365f;
    public Sprite[] sprites;
    public Sprite[] Hot;
    public Sprite[] Cool;
    public Sprite[] defult;
    public Brick brickPrefab;
    public int theme = 1;
    private Color[] BrickColors;
    public Color[] BrickColorsHOT;
    public Color[] BrickColorsCool;
    public Color[] defultBrick;
    public int IntialBricksCount { get; set; }
    public List<Brick> ReminigBricks { get; set; }
    public List<int[,]> LevelData { get; set; }
    public static event Action OnLevelLoad;

    private bool Isgen = true;
    public int CurrentLevel;
    public AudioSource sound;

    public void Start()
    {
            this.brickContainer = new GameObject("BrickContainer");
            this.LevelData = this.LoadlevelData();
            
    }

    internal void NextLevel()
    {
        sound.Play();
        this.CurrentLevel++;
        if(this.CurrentLevel>= this.LevelData.Count)
        {
            GameManager.Instance.ShowWinScene();
        }
        else
        {
            this.LoadLevel(this.CurrentLevel);
        }
    }

    private void Update()
    {
        switch (theme)
        {
            case 1:
                BrickColors = BrickColorsHOT;
                sprites = Hot;
                break;
            case 2:
                BrickColors = BrickColorsCool;
                sprites = Cool;
                break;
            default:
                BrickColors = defultBrick;
                sprites = defult;
                break;
        }
        if (foundBricks == true)
        {
            brickContainer.SetActive(true);
            if (Isgen)
            {
                this.GenerateBricks();
                Debug.Log("gened");
                Isgen = !Isgen;
            }
        }
        else
        {
            brickContainer.SetActive(false);           
        }
       
    }

    public void LoadLevel(int level)
    {
        this.CurrentLevel = level;
        this.ClearRemaingBrick();
        this.GenerateBricks();
    }

    private void ClearRemaingBrick()
    {
       foreach(Brick brick in this.ReminigBricks.ToList())
        {
            Destroy(brick.gameObject);
        }
    }

    private void GenerateBricks()
    {
        this.ReminigBricks = new List<Brick>();
        int[,] currentLevelData = this.LevelData[this.CurrentLevel];
        float currentSpawnX = intialBrickSpwanPositionX;
        float currentSpawnY = intialBrickSpwanPositionY;
        float zShift = 0;
       
        for(int row = 0;row < this.maxRow; row++)
        {
            for(int col = 0;col < this.maxCols; col++)
            {
                int brickType = currentLevelData[row, col];
                if(brickType > 0)
                {
                    Brick newbrick = Instantiate(brickPrefab,new Vector3(currentSpawnX,currentSpawnY,0.0f-zShift),Quaternion.identity) as Brick;
                    newbrick.Init(brickContainer.transform, this.sprites[brickType  -1],this.BrickColors[brickType],brickType);
                    this.ReminigBricks.Add(newbrick);
                    zShift += 0.0001f;
                }
                currentSpawnX += shiftAmount;
                if (col + 1 == this.maxCols)
                {
                    currentSpawnX = intialBrickSpwanPositionX;
                }
            }
            currentSpawnY -= shiftAmount;

        }
        this.IntialBricksCount = this.ReminigBricks.Count;
        OnLevelLoad.Invoke();
    }

    private List<int[,]> LoadlevelData()
    {
        TextAsset text = Resources.Load("Levels") as TextAsset;
        string[] rows = text.text.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
        List<int[,]> levelData = new List<int[,]> { };
        int[,] currentLevel = new int[maxRow, maxCols];
        int currntRow = 0;
        for(int row =0;row < rows.Length; row++)
        {
            string line = rows[row];
            if (line.IndexOf("--") == -1)
            {
                string[] bricks = line.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for(int col = 0; col < bricks.Length; col++)
                {
                    currentLevel[currntRow, col] = int.Parse(bricks[col]);
                }
                currntRow++;
            }
            else
            {
                //end of level
                //matrix to the last and looping
                currntRow = 0;
                levelData.Add(currentLevel);
                currentLevel = new int[maxRow, maxCols];

            }
        }
        return levelData;
    }
}
