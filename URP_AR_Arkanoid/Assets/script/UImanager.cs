﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UImanager : MonoBehaviour
{
    public Text Target;
    public Text ScoreText;
    public Text LivesText;
    private void Awake()
    {
        Brick.onBrickDestruction += onBrickDestruction;
        BrickManager.OnLevelLoad += OnLevelLoad;
        GameManager.OnLiveLost += OnLiveLost;
    }
    public int Score { get; set; }
    private void Start()
    {
       
        OnLiveLost(GameManager.Instance.AvailibleLives);
    }

    private void OnLiveLost(int reminingLives)
    {
        LivesText.text = $"LIVES: {reminingLives}";
    }

    private void OnLevelLoad()
    {
        UpdateRemaningBrricksText();
        UpdateScoreText(0);
    }

    private void UpdateScoreText(int increament)
    {
        this.Score += increament;
        string scooreString = this.Score.ToString().PadLeft(5, '0');
        ScoreText.text = $"SCORE:{Environment.NewLine}{scooreString}";
    }

    private void onBrickDestruction(Brick obj)
    {
        UpdateRemaningBrricksText();
        UpdateScoreText(10);
    }

    private void UpdateRemaningBrricksText()
    {
        Target.text = $"TARGET:{Environment.NewLine}{BrickManager.Instance.ReminigBricks.Count}/{BrickManager.Instance.IntialBricksCount}";
    }
    private void OnDisable()
    {
        Brick.onBrickDestruction -= onBrickDestruction;
        BrickManager.OnLevelLoad -= OnLevelLoad;
    }
}
