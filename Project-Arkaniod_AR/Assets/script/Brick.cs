﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Brick : MonoBehaviour
{
    private BoxCollider2D boxCollider;
    private SpriteRenderer sr;
    public int hitpoint = 1;
    public ParticleSystem DestroyEffect;
    public static event Action<Brick> onBrickDestruction;
    private void Awake()
    {
        this.sr = this.GetComponent<SpriteRenderer>();
        this.boxCollider = this.GetComponent<BoxCollider2D>();
        ball.OnLightingBallEnable += OnLightingBallEnable;
        ball.OnLightingBallDisable += OnLightingBallDisable;
    }

    private void OnLightingBallDisable(ball obj)
    {
        if (this != null)
        {
            this.boxCollider.isTrigger = false;
        }
    }

    private void OnLightingBallEnable(ball obj)
    {
        if(this!= null)
        {
            this.boxCollider.isTrigger = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        ball ball = collision.gameObject.GetComponent<ball>();
        applyCollisionLogic(ball);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ball ball = collision.gameObject.GetComponent<ball>();
        applyCollisionLogic(ball);
    }
    private void applyCollisionLogic(ball ball)
    {
        this.hitpoint--;
        if (this.hitpoint <= 0 || (ball!= null && ball.islightingBall))
        {
            BrickManager.Instance.ReminigBricks.Remove(this);
            onBrickDestruction?.Invoke(this);
            OnBrickDestroy();
            SpawnDestroyEffect();
            Destroy(this.gameObject);
        }
        else
        {
            this.sr.sprite = BrickManager.Instance.sprites[this.hitpoint - 1];
        }
    }

    private void OnBrickDestroy()
    {
        float buffSpawnChance = UnityEngine.Random.Range(0,100f);
        float debuffSpawnChance = UnityEngine.Random.Range(0, 100f);
        bool alreadySpawned = false;
        if(buffSpawnChance<= CollectableManager.Instance.buffchance)
        {
            alreadySpawned = true;
            Collectable newBuff = this.SpawnCollectable(true);
        }
        if(debuffSpawnChance<= CollectableManager.Instance.debuffchance && !alreadySpawned)
        {
            Collectable newDeBuff = this.SpawnCollectable(false);
        }
    }

    private Collectable SpawnCollectable(bool isBuff)
    {
        List<Collectable> collection;
        if (isBuff)
        {
            collection = CollectableManager.Instance.AvaliableBuffs;
        }
        else
        {
            collection = CollectableManager.Instance.AvaliableDeBuffs;
        }
        int buffIndex = UnityEngine.Random.Range(0, collection.Count);
        Collectable prefab = collection[buffIndex];
        Collectable newCollectable = Instantiate(prefab, this.transform.position, Quaternion.identity) as Collectable;

        return newCollectable;
    }

    private void SpawnDestroyEffect()
    {
        Vector3 brickpos = gameObject.transform.position;
        Vector3 spawnPos = new Vector3(brickpos.x, brickpos.y, brickpos.z-0.2f);
        GameObject effect = Instantiate(DestroyEffect.gameObject, spawnPos, Quaternion.identity);

        MainModule nm = effect.GetComponent<ParticleSystem>().main;
        nm.startColor = this.sr.color;
        Destroy(effect, DestroyEffect.main.startLifetime.constant);
    }

    internal void Init(Transform transform, Sprite sprite, Color color, int hitpoints)
    {
        this.transform.SetParent(transform.transform);
        this.sr.sprite = sprite;
        this.sr.color = color;
        this.hitpoint = hitpoints;
    }
    private void OnDisable()
    {
        ball.OnLightingBallEnable -= OnLightingBallEnable;
        ball.OnLightingBallDisable -= OnLightingBallDisable;
    }
}
