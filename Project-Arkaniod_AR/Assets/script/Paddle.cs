﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    #region sigleton

    private static Paddle _instance;
    public static Paddle Instance => _instance;

  

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion
    
    private Camera maincamera;
    private float paddleInitialY;
    private float defailtPaddleWidthInPixels = 200;
    private SpriteRenderer sr;
    public GameObject ImageTrack;
    public float extendShrinkDuration = 10f;
    public float paddleWidth = 2;
    public float paddleHight = 0.28f;
    private BoxCollider2D boxCol;
    public bool PaddleTransfroming { get;  set; }
    private void Start()
    {
        maincamera = FindObjectOfType<Camera>();
        paddleInitialY = this.transform.position.y;
        sr = GetComponent<SpriteRenderer>();
        boxCol = GetComponent<BoxCollider2D>();
    }
    // Update is called once per frame
    void Update()
    {
        PaddleMovement();
    }
    public void StartWidthAnimation(float newWidth)
    {
        StartCoroutine(AnimationPaddleWidth(newWidth));
    }

    public IEnumerator AnimationPaddleWidth(float width)
    {
        this.PaddleTransfroming = true;
        this.StartCoroutine(ResetPaddleWidthAfterTime(this.extendShrinkDuration));

        if (width > this.sr.size.x)
        {
            float currentWidth = this.sr.size.x;
            while (currentWidth < width)
            {
                currentWidth += Time.deltaTime * 2;
                this.sr.size = new Vector2(currentWidth, paddleHight);
                boxCol.size = new Vector2(currentWidth, paddleHight); ;
                yield return null;
            }

        }
        else
        {
            float currentWidth = this.sr.size.x;
            while(currentWidth > width)
            {
                currentWidth -= Time.deltaTime * 2;
                this.sr.size = new Vector2(currentWidth, paddleHight);
                boxCol.size = new Vector2(currentWidth, paddleHight); ;
                yield return null;
            }
        }
        this.PaddleTransfroming = false;
    }

    private IEnumerator ResetPaddleWidthAfterTime(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        this.StartWidthAnimation(this.paddleWidth);
    }

    private void PaddleMovement()
    {
           // float paddleShift = (defailtPaddleWidthInPixels-((defailtPaddleWidthInPixels/2)*this.sr.size.x)) / 2;
          //Debug.Log(paddleShift);
          float leftClamp =-1.5f;
          float rightClamp = 1.5f;
          float arPositionPixels = Mathf.Clamp(100* ImageTrack.transform.position.x, leftClamp, rightClamp);
          //float arPositionWorldX = maincamera.ScreenToViewportPoint(new Vector3(arPositionPixels,0,0)).x;
          //float ar = maincamera.WorldToScreenPoint(new Vector3(arPositionPixels, 0, 0)).x;
          this.transform.position = new Vector3(arPositionPixels, paddleInitialY, 0);
        //this.transform.position = new Vector3(100 * ImageTrack.transform.position.x, paddleInitialY, 0);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            Rigidbody2D ballRb = collision.gameObject.GetComponent<Rigidbody2D>();
            Vector3 hitpoint = collision.contacts[0].point;
            Vector3 paddleCenter = new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y);

            ballRb.velocity = Vector3.zero;
            float difference = paddleCenter.x - hitpoint.x;
            if (hitpoint.x < paddleCenter.x)
            {
                ballRb.AddForce(new Vector2(-(Mathf.Abs(difference * 200)),BallManager.Instance.ballspeed));
            }
            else
            {
                ballRb.AddForce(new Vector2((Mathf.Abs(difference * 200)), BallManager.Instance.ballspeed));
            }
        }
    }
}
