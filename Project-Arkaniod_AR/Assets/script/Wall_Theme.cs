﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wall_Theme : MonoBehaviour
{
    public GameObject wall1, wall2, wall3,panel;
    public Color HotTheme;
    public Color DefultTheme;
    public Color CoolTheme;
    private Renderer sr1,sr2,sr3;
    private Image sr4;
    // Start is called before the first frame update
    void Start()
    {
        sr1 = wall1.GetComponent<Renderer>();
        sr2 = wall2.GetComponent<Renderer>();
        sr3 = wall3.GetComponent<Renderer>();
        sr4 = panel.GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (BrickManager.Instance.theme)
        {
            case 1:
                this.sr1.material.color = HotTheme;
                this.sr2.material.color = HotTheme;
                this.sr3.material.color = HotTheme;
                this.sr4.color = HotTheme;
                break;
            case 2:
                this.sr1.material.color = CoolTheme;
                this.sr2.material.color = CoolTheme;
                this.sr3.material.color = CoolTheme;
                this.sr4.color = CoolTheme;
                break;
            default:
                this.sr1.material.color = DefultTheme;
                this.sr2.material.color = DefultTheme;
                this.sr3.material.color = DefultTheme;
                this.sr4.color = DefultTheme;
                break;

        }
    }
}
