﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    #region sigleton

    private static BallManager _instance;
    public static BallManager Instance => _instance;
    private Renderer sr;
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    #endregion

    [SerializeField]
    private ball ballPrefab;
    private ball intaialBall;
    private Rigidbody2D intaialBallRB;
    public float ballspeed = 250;
    public List<ball> balls { get; set; }
    private void Start()
    {
        InitialBall();
        

    }
    private void Update()
    {
        if (!GameManager.Instance.IsgameStart)
        {
            Vector3 paddlePosition = Paddle.Instance.gameObject.transform.position;
            Vector3 ballPosittion = new Vector3(paddlePosition.x, paddlePosition.y + 0.27f, 0);
            intaialBall.transform.position = ballPosittion;
            sr = intaialBall.GetComponentInChildren<Renderer>();
            switch (BrickManager.Instance.theme)
            {
                case 1:
                    this.sr.material.color = Color.red;
                    break;
                case 2:
                    this.sr.material.color = Color.cyan;
                    break;
                default:
                    this.sr.material.color = Color.white;
                    break;

            }
            if (Input.GetKey(KeyCode.Space))
            {
                intaialBallRB.isKinematic = false;
                intaialBallRB.AddForce(new Vector2(0, ballspeed));
                GameManager.Instance.IsgameStart = true;
            }
        }
    }

    internal void ResetBalls()
    {
        foreach(var ball in this.balls.ToList())
        {
            Destroy(ball.gameObject);
        }
        InitialBall();
    }

    private void InitialBall()
    {
        Vector3 paddlePosition = Paddle.Instance.gameObject.transform.position;
        Vector3 startingPosition = new Vector3(paddlePosition.x, paddlePosition.y+.27f,0);
        intaialBall = Instantiate(ballPrefab, startingPosition, Quaternion.identity);
        intaialBallRB = intaialBall.GetComponent<Rigidbody2D>();
        this.balls = new List<ball>
        {
            intaialBall
        };

    }
    public void SpawnBalls(Vector3 position, int count,bool islightingBall)
    {
        for (int i = 0; i < count; i++)
        {
            ball spawnball = Instantiate(ballPrefab, position, Quaternion.identity) as ball;
            if (islightingBall)
            {
                spawnball.StartLightingBall();
            }
            Rigidbody2D spawnballRb = spawnball.GetComponent<Rigidbody2D>();
            sr = spawnball.GetComponentInChildren<Renderer>();
            switch (BrickManager.Instance.theme)
            {
                case 1:
                    this.sr.material.color= Color.red;
                    break;
                case 2:
                    this.sr.material.color = Color.cyan;
                    break;
                default:
                    this.sr.material.color = Color.white;
                    break;

            }
            spawnballRb.isKinematic = false;
            spawnballRb.AddForce(new Vector2(0, ballspeed));
            this.balls.Add(spawnball);
            
         }
    }

}
