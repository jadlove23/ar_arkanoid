﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtendorShronk : Collectable
{
    public float NewWidth = 2.5f;
    protected override void Applyeffect()
    {
        if(Paddle.Instance != null && !Paddle.Instance.PaddleTransfroming)
        {
            Paddle.Instance.StartWidthAnimation(NewWidth);
        }
    }
}
