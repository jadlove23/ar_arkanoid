﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lightingBall : Collectable
{
    protected override void Applyeffect()
    {
        foreach(var ball in BallManager.Instance.balls)
        {
            ball.StartLightingBall();
        }
        
    }

    
}
