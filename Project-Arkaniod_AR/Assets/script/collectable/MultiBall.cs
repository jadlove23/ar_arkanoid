﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MultiBall : Collectable
{
    protected override void Applyeffect()
    {
       foreach(ball ball in BallManager.Instance.balls.ToList())
        {
            BallManager.Instance.SpawnBalls(ball.gameObject.transform.position,2,ball.islightingBall);
        }
    }
}
