﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableManager : MonoBehaviour
{
    #region sigleton

    private static CollectableManager _instance;
    public static CollectableManager Instance => _instance;
    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    #endregion 
    public List<Collectable> AvaliableBuffs;
    public List<Collectable> AvaliableDeBuffs;

    [Range(0,100)]
    public float buffchance;
    [Range(0, 100)]
    public float debuffchance;
}


